﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Map
{
    class NetAlumnes
    {
        private static ProcessStartInfo processStartInfo;

        static NetAlumnes ()
        {
            processStartInfo = new ProcessStartInfo();
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = true;
        }

        public Boolean login(String username, String password)
        {
            if (isLoggedIn()) logout();

            /*Process login = Process.Start("net.exe", "use H: \\\\storealumnes\\" + username + " "
                + password + " /user:" + username);*/
            //login.WaitForExit();
            int code = executeProcess("net.exe", "use H: \\\\storealumnes\\" + username + " " 
                                       + password + " /user:" + username);
           return code == 0;
        }

        public void logout()
        {
            executeProcess("net.exe", "use H: /delete /yes");
        }

        public Boolean isLoggedIn()
        {
            string drive = Path.GetPathRoot("h:");
            return Directory.Exists(drive);
        }

        public void browse()
        {
            executeProcess("explorer.exe", "H:");
        }

        private int executeProcess(String filename, String args)
        {
            processStartInfo.FileName = filename;
            processStartInfo.Arguments = args;
            Process process = new Process();
            process.StartInfo = processStartInfo;
            process.Start();
            process.WaitForExit();
            return process.ExitCode;
        }
    }
}
