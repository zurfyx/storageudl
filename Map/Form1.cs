﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Map
{
    public partial class Form1 : Form
    {
        NetAlumnes net;

        public Form1()
        {
            net = new NetAlumnes();
            InitializeComponent();
        }

        private void login_Click(object sender, EventArgs e)
        {
            filterUsername();
            loginNet();
            if (net.isLoggedIn())
            {
                updateFormStatus();
            } else {
                MessageBox.Show("Error! Revisa usuari / contrasenya i connexió a xarxa.");
            }
        }

        private void logout_Click(object sender, EventArgs e)
        {
            disconnectNet();
            updateFormStatus();
            clearInput();
        }
        
        private void browse_Click(object sender, EventArgs e)
        {
            net.browse();
        }

        private void Form1_Load ( object sender, EventArgs e)
        {

        }

        private void username_TextChanged(object sender, EventArgs e)
        {
            updateLoginButtonEnabled();
        }

        private void password_TextChanged(object sender, EventArgs e)
        {
            updateLoginButtonEnabled();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            updateFormStatus();
        }

        private void updateFormStatus()
        {
            if (net.isLoggedIn())
            {
                browse.Visible = true;
                logout.Visible = true;
                login.Visible = false;
                username.ReadOnly = true;
                password.ReadOnly = true;
            } else {
                login.Visible = true;
                browse.Visible = false;
                logout.Visible = false;                
                updateLoginButtonEnabled();
                username.ReadOnly = false;
                password.ReadOnly = false;
            }
        }

        private void updateLoginButtonEnabled()
        {
            login.Enabled = (username.Text != "" && password.Text != "") ? true : false;
        }

        private void clearInput()
        {
            username.Text = "";
            password.Text = "";
        }

        private void filterUsername()
        {
            int index;
            if ((index = username.Text.IndexOf('@')) != -1){
                username.Text = username.Text.Substring(0, index);
            }
        }

        private void loginNet()
        {
            login.Text = "Connectant...";
            net.login(username.Text, password.Text);
            login.Text = "Connectar";
        }

        private void disconnectNet()
        {
            logout.Text = "Desconnectant...";
            net.logout();
            logout.Text = "Desconnectar";
        }
    }
}
