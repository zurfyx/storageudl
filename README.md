# UDL Carpeta Personal Alumnes

## Requisits
- [Visual Studio 2015](https://www.visualstudio.com/en-us/downloads/download-visual-studio-vs.aspx) (altres versions no provades)
- (Opcional) [Git per a Windows](https://git-scm.com/download/win)

## Instruccions

- Obrir pel Map.sln

## Versió producció

Compilar amb Release. Output a: \bin\release\Map.exe

## Desenvolupament

Arxius rellevants:

- Logic\NetAlumnes.cs: Funcions de connexió amb la unitat d'emmagatzemament de l'UDL.
- Form1.Designer.cs: Elements del pla (layout) i la seva configuració.
- Form1.resx: Recursos (imatges)
- Program.cs: Inicialitzador de l'aplicació (crea nova instància de Form1).

Preferible compilar amb Debug.
